# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.2.1](https://gitlab.com/bot-by/message-bundle/-/tags/1.2.1) - 2020-07-28
### Fixed
- Apache License, text
- NOTICE only in META-INF, no copy of LICENSE

## [1.2.0](https://gitlab.com/bot-by/message-bundle/-/tags/1.2.0) - 2020-07-09
### Added
- Get all bundle names

## [1.1.1](https://gitlab.com/bot-by/message-bundle/-/tags/1.1.1) - 2020-07-08
### Fixed
- Close a resource reader as JSONTokener requires

## [1.1.0](https://gitlab.com/bot-by/message-bundle/-/tags/1.1.0) - 2020-07-03
### Added
- Optional bundle method.

## [1.0.0](https://gitlab.com/bot-by/message-bundle/-/tags/1.0.0) - 2020-07-02
### Added
- Initial release.
