package uk.bot_by.util;

@FunctionalInterface
interface TerFunction<T, U, V, R> {

	R apply(T t, U u, V v);

}
