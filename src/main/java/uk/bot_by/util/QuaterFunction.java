package uk.bot_by.util;

@FunctionalInterface
interface QuaterFunction<T, U, V, W, R> {

	R apply(T t, U u, V v, W w);

}
