/*
 * Copyright 2020 Witalij Berdinskich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.bot_by.util;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONPointerException;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;

/**
 * ResourceBundle alternative with hierarchy and JSON pointers.
 *
 * <p>
 * It works like a proxy for {@link JSONObject}.
 * Read more on github:<a href="https://github.com/stleary/JSON-java">stleary/JSON-java</a>
 * </p>
 *
 * @author Witalij Berdinskich
 * @see JSONArray
 * @see JSONObject
 * @see org.json.JSONPointer
 * @see <a href="https://tools.ietf.org/html/rfc6901">RFC 6901: JSON Pointer</a>
 * @since 1.0.0
 */
public final class MessageBundle {

	private static final String BUNDLES_ARE_NOT_FOUND = "Bundles are not found";
	private static final String IS_MISSED = " is missed";
	private static final String JSON = ".json";
	private static final String LOADED_MESSAGES = "Loaded bundles: {}. Default bundle: {}.";
	private static final String REGEX_DOT = "\\.";
	private static final String SLASH = "/";

	private static final Logger LOGGER = LoggerFactory.getLogger(MessageBundle.class);
	public static final String COULD_NOT_EVALUATE_THE_POINTER = "Could not evaluate the pointer: ";

	private final String bundleName;
	private final JSONObject messages;
	private final JSONObject messageBundle;

	/**
	 * Look for the resource file <code>parent/package/ClassName.json</code> and create new instance.
	 * <p>
	 * It should have one message bungle at least. The default charset is UTF-8.
	 *
	 * @param baseClass A class whose full name is used as a filepath of a JSON resource.
	 * @return The message bundle.
	 * @throws JSONException          If a resource file contains invalid JSON.
	 * @throws MessageBundleException If a resource file is not found or empty or other I/O error occurs.
	 * @see #getInstance(String, ClassLoader, Charset)
	 */
	public static @NotNull MessageBundle getInstance(@NotNull Class<?> baseClass)
			throws JSONException, MessageBundleException {
		return getInstance(baseClass.getPackageName().replaceAll(REGEX_DOT, SLASH) + SLASH + baseClass.getSimpleName() + JSON);
	}

	/**
	 * Look for a JSON file and create new instance.
	 * <p>
	 * It should have one message bungle at least. The default charset is UTF-8.
	 *
	 * @param baseName A filepath of a JSON resource.
	 * @return The message bundle.
	 * @throws JSONException          If the resource file contains invalid JSON.
	 * @throws MessageBundleException If a resource file is not found or empty or other I/O error occurs.
	 * @see #getInstance(String, ClassLoader, Charset)
	 */
	public static @NotNull MessageBundle getInstance(String baseName)
			throws JSONException, MessageBundleException {
		return getInstance(baseName, MessageBundle.class.getClassLoader());
	}

	/**
	 * Look for a file and create new instance.
	 * <p>
	 * It should have one message bungle at least. The default charset is UTF-8.
	 *
	 * @param baseName    A filepath of a JSON resource.
	 * @param classLoader A classloader.
	 * @return The message bundle.
	 * @throws JSONException          If the resource file contains invalid JSON.
	 * @throws NullPointerException   If a base name or charset are <em>null</em>.
	 * @throws MessageBundleException If a resource file is not found or empty or other I/O error occurs.
	 * @see #getInstance(String, ClassLoader, Charset)
	 */
	public static @NotNull MessageBundle getInstance(String baseName, ClassLoader classLoader)
			throws JSONException, NullPointerException, MessageBundleException {
		return getInstance(baseName, classLoader, StandardCharsets.UTF_8);
	}

	/**
	 * Look for a file and create new instance.
	 * <p>
	 * It should have one message bungle at least.
	 *
	 * @param baseName    A filepath of a JSON resource.
	 * @param classLoader A classloader.
	 * @param charset     A charset of a JSON resource.
	 * @return The message bundle.
	 * @throws JSONException          If the resource file contains invalid JSON.
	 * @throws NullPointerException   If a base name or charset are <em>null</em>.
	 * @throws MessageBundleException If a resource file is not found or empty or other I/O error occurs.
	 */
	@Contract("_, _, _ -> new")
	public static @NotNull MessageBundle getInstance(String baseName, @NotNull ClassLoader classLoader, Charset charset)
			throws JSONException, NullPointerException, MessageBundleException {
		Objects.requireNonNull(baseName, "Base name should not be null");
		Objects.requireNonNull(charset, "Charset should not be null");

		LOGGER.debug("Looking for {}", baseName);

		try (InputStream messageStream = classLoader.getResourceAsStream(baseName)) {
			if (Objects.isNull(messageStream)) {
				throw new MessageBundleException(baseName + IS_MISSED);
			}

			JSONObject messages = new JSONObject(new JSONTokener(new InputStreamReader(messageStream, charset)));

			messageStream.close();
			if (messages.keySet().isEmpty()) {
				throw new MessageBundleException(BUNDLES_ARE_NOT_FOUND);
			}

			return new MessageBundle(messages, messages.keySet().iterator().next());
		} catch (IOException exception) {
			throw new MessageBundleException(exception.getMessage(), exception);
		}
	}

	private MessageBundle(@NotNull JSONObject messages, String bundleName) {
		this.messages = messages;
		this.bundleName = bundleName;
		this.messageBundle = messages.getJSONObject(bundleName);
		LOGGER.debug(LOADED_MESSAGES, messages.keySet(), bundleName);
	}

	/**
	 * Create a copy with new active bundle.
	 *
	 * @param bundleName A name of another message bundle.
	 * @return The message bundle.
	 * @throws IllegalArgumentException If message bundle is not found.
	 */
	@Contract("_ -> new")
	public @NotNull MessageBundle getBundle(String bundleName) throws IllegalArgumentException {
		if (messages.has(bundleName)) {
			return new MessageBundle(messages, bundleName);
		}

		throw new IllegalArgumentException("Bundle is not found: " + bundleName);
	}

	/**
	 * Get an optional copy with new active bundle.
	 * <p>
	 * If there is no bundle for this name it returns itself.
	 *
	 * @param bundleName A name of another message bundle.
	 * @return The message bundles.
	 */
	public MessageBundle optBundle(String bundleName) {
		return messages.has(bundleName) ? new MessageBundle(messages, bundleName) : this;
	}

	/**
	 * Gets a name of active bundle.
	 *
	 * @return The name of active bundle.
	 */
	public String getBundleName() {
		return bundleName;
	}

	/**
	 * Gets all bundle names.
	 *
	 * @return The set of bundle names.
	 */
	public Set<String> getAllBundleNames() {
		return messages.keySet();
	}

	/**
	 * Gets a BigDecimal value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to BigDecimal.
	 * @see JSONObject#getBigDecimal(String)
	 */
	public BigDecimal getBigDecimal(String key) throws JSONException {
		return messageBundle.getBigDecimal(key);
	}

	/**
	 * Gets an optional BigDecimal value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a BigDecimal.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optBigDecimal(String, BigDecimal)
	 */
	public BigDecimal optBigDecimal(String key, BigDecimal defaultValue) {
		return messageBundle.optBigDecimal(key, defaultValue);
	}

	/**
	 * Evaluates a BigDecimal value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to BigDecimal; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getBigDecimal(String)
	 */
	public BigDecimal queryBigDecimal(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getBigDecimal, JSONObject::getBigDecimal);
	}

	/**
	 * Evaluates an optional BigDecimal value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a BigDecimal.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optBigDecimal(String, BigDecimal)
	 */
	public BigDecimal optQueryBigDecimal(String pointer, BigDecimal defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optBigDecimal, JSONObject::optBigDecimal);
	}

	/**
	 * Gets a BigInteger value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to BigDecimal.
	 * @see JSONObject#getBigInteger(String)
	 */
	public BigInteger getBigInteger(String key) throws JSONException {
		return messageBundle.getBigInteger(key);
	}

	/**
	 * Gets an optional BigInteger value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a BigInteger.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optBigInteger(String, BigInteger)
	 */
	public BigInteger optBigInteger(String key, BigInteger defaultValue) {
		return messageBundle.optBigInteger(key, defaultValue);
	}

	/**
	 * Evaluates a BigInteger value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to BigInteger; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getBigInteger(String)
	 */
	public BigInteger queryBigInteger(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getBigInteger, JSONObject::getBigInteger);
	}

	/**
	 * Evaluates an optional BigInteger value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a BigInteger.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optBigInteger(String, BigInteger)
	 */
	public BigInteger optQueryBigInteger(String pointer, BigInteger defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optBigInteger, JSONObject::optBigInteger);
	}

	/**
	 * Gets a boolean value associated with a key.
	 *
	 * @param key A key string.
	 * @return The boolean value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to boolean.
	 * @see JSONObject#getBoolean(String)
	 */
	public boolean getBoolean(String key) throws JSONException {
		return messageBundle.getBoolean(key);
	}

	/**
	 * Gets an optional boolean value associated with a key, of the <code>false</code> if there is no such key or its value is
	 * not a boolean.
	 *
	 * @param key A key string.
	 * @return The boolean value.
	 * @see JSONObject#optBoolean(String)
	 */
	public boolean optBoolean(String key) {
		return messageBundle.optBoolean(key);
	}

	/**
	 * Gets an optional boolean value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a boolean.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The boolean value.
	 * @see JSONObject#optBoolean(String, boolean)
	 */
	public boolean optBoolean(String key, boolean defaultValue) {
		return messageBundle.optBoolean(key, defaultValue);
	}

	/**
	 * Evaluates a boolean value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The boolean value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to boolean; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getBoolean(String)
	 */
	public boolean queryBoolean(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getBoolean, JSONObject::getBoolean);
	}

	/**
	 * Evaluates an optional boolean value associated with a pointer, of the <code>false</code> if there is no such key or
	 * its value is not a boolean.
	 *
	 * @param pointer A JSON pointer.
	 * @return The boolean value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optBoolean(String)
	 */
	public boolean optQueryBoolean(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optBoolean, JSONObject::optBoolean);
	}

	/**
	 * Evaluates an optional boolean value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a boolean.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The boolean value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optBoolean(String, boolean)
	 */
	public boolean optQueryBoolean(String pointer, boolean defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optBoolean, JSONObject::optBoolean);
	}

	/**
	 * Gets a double value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to double.
	 * @see JSONObject#getDouble(String)
	 */
	public double getDouble(String key) throws JSONException {
		return messageBundle.getDouble(key);
	}

	/**
	 * Gets an optional double value associated with a key, of the <code>Double.NaN</code> if there is no such key or its value is
	 * not a double.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @see JSONObject#optDouble(String)
	 */
	public double optDouble(String key) {
		return messageBundle.optDouble(key);
	}

	/**
	 * Gets an optional double value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a double.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optDouble(String, double)
	 */
	public double optDouble(String key, double defaultValue) {
		return messageBundle.optDouble(key, defaultValue);
	}

	/**
	 * Evaluates a double value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to double; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getBoolean(String)
	 */
	public double queryDouble(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getDouble, JSONObject::getDouble);
	}

	/**
	 * Evaluates an optional double value associated with a pointer, of the <code>Double.NaN</code> if there is no such key or
	 * its value is not a double.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optDouble(String)
	 */
	public double optQueryDouble(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optDouble, JSONObject::optDouble);
	}

	/**
	 * Evaluates an optional double value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a double.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optDouble(String, double)
	 */
	public double optQueryDouble(String pointer, double defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optDouble, JSONObject::optDouble);
	}

	/**
	 * Gets an enum value associated with a key.
	 *
	 * @param enumClass An enum class.
	 * @param key       A key string.
	 * @param <E>       An enum type class.
	 * @return The enum value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to enum.
	 * @see JSONObject#getEnum(Class, String)
	 */
	public <E extends Enum<E>> E getEnum(Class<E> enumClass, String key) throws JSONException {
		return messageBundle.getEnum(enumClass, key);
	}

	/**
	 * Gets an optional enum value associated with a key, of the <code>null</code> if there is no such key or its value is
	 * not an enum.
	 *
	 * @param enumClass An enum class.
	 * @param key       A key string.
	 * @param <E>       An enum type class.
	 * @return The enum value.
	 * @see JSONObject#optEnum(Class, String)
	 */
	public <E extends Enum<E>> E optEnum(Class<E> enumClass, String key) {
		return messageBundle.optEnum(enumClass, key);
	}

	/**
	 * Gets an optional enum value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not an enum.
	 *
	 * @param enumClass    An enum class.
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @param <E>          An enum type class.
	 * @return The enum value.
	 * @see JSONObject#optEnum(Class, String, Enum)
	 */
	public <E extends Enum<E>> E optEnum(Class<E> enumClass, String key, E defaultValue) {
		return messageBundle.optEnum(enumClass, key, defaultValue);
	}

	/**
	 * Evaluates an enum value associated with a pointer.
	 *
	 * @param enumClass An enum class.
	 * @param pointer   A JSON pointer.
	 * @param <E>       An enum type class.
	 * @return The enum value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to enum; if an error occurs
	 *                                  during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getEnum(Class, String)
	 */
	public <E extends Enum<E>> E queryEnum(Class<E> enumClass, @NotNull String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryTypeValue(enumClass, pointer, JSONArray::getEnum, JSONObject::getEnum);
	}

	/**
	 * Evaluates an optional enum value associated with a pointer, of the <code>null</code> if there is no such key or
	 * its value is not an enum.
	 *
	 * @param enumClass An enum class.
	 * @param pointer   A JSON pointer.
	 * @param <E>       An enum type class.
	 * @return The enum value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optEnum(Class, String)
	 */
	public <E extends Enum<E>> E optQueryEnum(Class<E> enumClass, @NotNull String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryTypeValue(enumClass, pointer, JSONArray::optEnum, JSONObject::optEnum);
	}

	/**
	 * Evaluates an optional enum value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not an enum.
	 *
	 * @param enumClass    An enum class.
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @param <E>          An enum type class.
	 * @return The enum value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optEnum(Class, String, Enum)
	 */
	public <E extends Enum<E>> E optQueryEnum(Class<E> enumClass, @NotNull String pointer, E defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryTypeValueWithDefault(enumClass, pointer, defaultValue, JSONArray::optEnum, JSONObject::optEnum);
	}

	/**
	 * Gets a float value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to float.
	 * @see JSONObject#getFloat(String)
	 */
	public float getFloat(String key) throws JSONException {
		return messageBundle.getFloat(key);
	}

	/**
	 * Gets an optional float value associated with a key, of the <code>Float.NaN</code> if there is no such key or its value is
	 * not a float.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @see JSONObject#optFloat(String)
	 */
	public float optFloat(String key) {
		return messageBundle.optFloat(key);
	}

	/**
	 * Gets an optional float value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a float.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optFloat(String, float)
	 */
	public float optFloat(String key, float defaultValue) {
		return messageBundle.optFloat(key, defaultValue);
	}

	/**
	 * Evaluates a float value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to float; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getFloat(String)
	 */
	public float queryFloat(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getFloat, JSONObject::getFloat);
	}

	/**
	 * Evaluates an optional float value associated with a pointer, of the <code>Float.NaN</code> if there is no such key or
	 * its value is not a float.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optQuery(String)
	 */
	public float optQueryFloat(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optFloat, JSONObject::optFloat);
	}

	/**
	 * Evaluates an optional float value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a float.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optFloat(String, float)
	 */
	public float optQueryFloat(String pointer, float defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optFloat, JSONObject::optFloat);
	}

	/**
	 * Gets an integer value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to integer.
	 * @see JSONObject#getInt(String)
	 */
	public int getInt(String key) throws JSONException {
		return messageBundle.getInt(key);
	}

	/**
	 * Gets an optional integer value associated with a key, of the <code>0</code> if there is no such key or its value is
	 * not an integer.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @see JSONObject#optInt(String)
	 */
	public int optInt(String key) {
		return messageBundle.optInt(key);
	}

	/**
	 * Gets an optional integer value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not an integer.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optInt(String, int)
	 */
	public int optInt(String key, int defaultValue) {
		return messageBundle.optInt(key, defaultValue);
	}

	/**
	 * Evaluates an integer value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to integer; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getInt(String)
	 */
	public int queryInt(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getInt, JSONObject::getInt);
	}

	/**
	 * Evaluates an optional integer value associated with a pointer, of the <code>0</code> if there is no such key or
	 * its value is not an integer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optInt(String)
	 */
	public int optQueryInt(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optInt, JSONObject::optInt);
	}

	/**
	 * Evaluates an optional integer value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not an integer.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optInt(String, int)
	 */
	public int optQueryInt(String pointer, int defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optInt, JSONObject::optInt);
	}

	/**
	 * Gets a long value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to long.
	 * @see JSONObject#getLong(String)
	 */
	public long getLong(String key) throws JSONException {
		return messageBundle.getLong(key);
	}

	/**
	 * Gets an optional long value associated with a key, of the <code>0</code> if there is no such key or its value is
	 * not a long.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @see JSONObject#optLong(String)
	 */
	public long optLong(String key) {
		return messageBundle.optLong(key);
	}

	/**
	 * Gets an optional long value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a long.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optLong(String, long)
	 */
	public long optLong(String key, long defaultValue) {
		return messageBundle.optLong(key, defaultValue);
	}

	/**
	 * Evaluates a long value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to long; if an error occurs
	 *                                  during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getLong(String)
	 */
	public long queryLong(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getLong, JSONObject::getLong);
	}

	/**
	 * Evaluates an optional long value associated with a pointer, of the <code>0</code> if there is no such key or
	 * its value is not a long.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optLong(String)
	 */
	public long optQueryLong(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optLong, JSONObject::optLong);
	}

	/**
	 * Evaluates an optional long value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a long.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optLong(String)
	 */
	public long optQueryLong(String pointer, long defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optLong, JSONObject::optLong);
	}

	/**
	 * Gets a number value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to number.
	 * @see JSONObject#getNumber(String)
	 */
	public Number getNumber(String key) throws JSONException {
		return messageBundle.getNumber(key);
	}

	/**
	 * Gets an optional number value associated with a key, of the <code>null</code> if there is no such key or its value is
	 * not a number.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @see JSONObject#optNumber(String)
	 */
	public Number optNumber(String key) {
		return messageBundle.optNumber(key);
	}

	/**
	 * Gets an optional number value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not a number.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @see JSONObject#optNumber(String, Number)
	 */
	public Number optNumber(String key, Number defaultValue) {
		return messageBundle.optNumber(key, defaultValue);
	}

	/**
	 * Evaluates a number value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to number; if an error
	 *                                  occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getNumber(String)
	 */
	public Number queryNumber(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getNumber, JSONObject::getNumber);
	}

	/**
	 * Evaluates an optional number value associated with a pointer, of the <code>null</code> if there is no such key or
	 * its value is not a number.
	 *
	 * @param pointer A JSON pointer.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optNumber(String)
	 */
	public Number optQueryNumber(String pointer)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValue(pointer, JSONArray::optNumber, JSONObject::optNumber);
	}

	/**
	 * Evaluates an optional number value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a number.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The numeric value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optNumber(String, Number)
	 */
	public Number optQueryNumber(String pointer, Number defaultValue)
			throws IllegalArgumentException, JSONPointerException, MessageBundleException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optNumber, JSONObject::optNumber);
	}

	/**
	 * Gets a string value associated with a key.
	 *
	 * @param key A key string.
	 * @return The numeric value.
	 * @throws JSONException If the key is not found or if the value cannot be converted to string.
	 * @see JSONObject#getString(String)
	 */
	public String getString(String key) throws JSONException {
		return messageBundle.getString(key);
	}

	/**
	 * Gets an optional string value associated with a key, of the <code>&quot;&quot;</code> (empty string) if there is no such key or
	 * its
	 * value is
	 * not an string.
	 *
	 * @param key A key string.
	 * @return The string value.
	 * @see JSONObject#optString(String)
	 */
	public String optString(String key) {
		return messageBundle.optString(key);
	}

	/**
	 * Gets an optional string value associated with a key, of the default value <code>defaultValue</code> if there is no
	 * such key or its value is not an string.
	 *
	 * @param key          A key string.
	 * @param defaultValue A default value.
	 * @return The string value.
	 * @see JSONObject#optString(String, String)
	 */
	public String optString(String key, String defaultValue) {
		return messageBundle.optString(key, defaultValue);
	}

	/**
	 * Evaluates an string value associated with a pointer.
	 *
	 * @param pointer A JSON pointer.
	 * @return The string value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONException            If the key is not found or if the value cannot be converted to string; if an error occurs during
	 *                                  evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#getString(String)
	 */
	public String queryString(String pointer)
			throws IllegalArgumentException, JSONException, MessageBundleException {
		return queryValue(pointer, JSONArray::getString, JSONObject::getString);
	}

	/**
	 * Evaluates an optional string value associated with a pointer, of the default value <code>&quot;&quot;</code> (empty
	 * string) if there is no such key or its value is not a string.
	 *
	 * @param pointer A JSON pointer.
	 * @return The string value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optString(String)
	 */
	public String optQueryString(String pointer) throws IllegalArgumentException, JSONPointerException {
		return queryValue(pointer, JSONArray::optString, JSONObject::optString);
	}

	/**
	 * Evaluates an optional string value associated with a pointer, of the default value <code>defaultValue</code> if
	 * there is no such key or its value is not a string.
	 *
	 * @param pointer      A JSON pointer.
	 * @param defaultValue A default value.
	 * @return The string value.
	 * @throws IllegalArgumentException If a pointer is wrong.
	 * @throws JSONPointerException     If an error occurs during evaluation.
	 * @throws MessageBundleException   If a pointer is wrong.
	 * @see JSONObject#query(String)
	 * @see JSONObject#optString(String, String)
	 */
	public String optQueryString(String pointer, String defaultValue) throws IllegalArgumentException, JSONPointerException {
		return queryValueWithDefault(pointer, defaultValue, JSONArray::optString, JSONObject::optString);
	}

	private <T> T queryValue(String pointer, BiFunction<JSONArray, Integer, T> getByIndex,
							 BiFunction<JSONObject, String, T> getByName) throws JSONPointerException, MessageBundleException {
		int lastTokenStarts = indexOfLastToken(pointer);
		String parentKey = pointer.substring(0, lastTokenStarts - 1);
		String lastKey = pointer.substring(lastTokenStarts);
		Object parent = findParent(parentKey, lastTokenStarts);
		T value;

		LOGGER.debug("Query: {}", pointer);
		if (parent instanceof JSONArray) {
			value = getByIndex.apply((JSONArray) parent, Integer.parseInt(lastKey));
		} else if (parent instanceof JSONObject) {
			value = getByName.apply((JSONObject) parent, lastKey);
		} else {
			throw new MessageBundleException(COULD_NOT_EVALUATE_THE_POINTER + parentKey);
		}

		return value;
	}

	private <T> T queryTypeValue(Class<T> type, String pointer, TerFunction<JSONArray, Class<T>, Integer, T> getByIndex,
								 TerFunction<JSONObject, Class<T>, String, T> getByName)
			throws JSONPointerException, MessageBundleException {
		int lastTokenStarts = indexOfLastToken(pointer);
		String parentKey = pointer.substring(0, lastTokenStarts - 1);
		String lastKey = pointer.substring(lastTokenStarts);
		Object parent = findParent(parentKey, lastTokenStarts);
		T value;

		LOGGER.debug("Query type: {}", pointer);
		if (parent instanceof JSONArray) {
			value = getByIndex.apply((JSONArray) parent, type, Integer.parseInt(lastKey));
		} else if (parent instanceof JSONObject) {
			value = getByName.apply((JSONObject) parent, type, lastKey);
		} else {
			throw new MessageBundleException(COULD_NOT_EVALUATE_THE_POINTER + parentKey);
		}

		return value;
	}

	private <T> T queryValueWithDefault(String pointer, T defaultValue, TerFunction<JSONArray, Integer, T, T> optByIndex,
										TerFunction<JSONObject, String, T, T> optByName)
			throws JSONPointerException, MessageBundleException {
		int lastTokenStarts = indexOfLastToken(pointer);
		String parentKey = pointer.substring(0, lastTokenStarts - 1);
		String lastKey = pointer.substring(lastTokenStarts);
		Object parent = findParent(parentKey, lastTokenStarts);
		T value;

		LOGGER.debug("Query with default {}: {}", defaultValue, pointer);
		if (parent instanceof JSONArray) {
			value = optByIndex.apply((JSONArray) parent, Integer.parseInt(lastKey), defaultValue);
		} else if (parent instanceof JSONObject) {
			value = optByName.apply((JSONObject) parent, lastKey, defaultValue);
		} else {
			value = defaultValue;
		}

		return value;
	}

	private <T> T queryTypeValueWithDefault(Class<T> type, String pointer, T defaultValue,
											QuaterFunction<JSONArray, Class<T>, Integer, T, T> getByIndex,
											QuaterFunction<JSONObject, Class<T>, String, T, T> getByName)
			throws JSONPointerException, MessageBundleException {
		int lastTokenStarts = indexOfLastToken(pointer);
		String parentKey = pointer.substring(0, lastTokenStarts - 1);
		String lastKey = pointer.substring(lastTokenStarts);
		Object parent = findParent(parentKey, lastTokenStarts);
		T value;

		LOGGER.debug("Query type with default {}: {}", defaultValue, pointer);
		if (parent instanceof JSONArray) {
			value = getByIndex.apply((JSONArray) parent, type, Integer.parseInt(lastKey), defaultValue);
		} else if (parent instanceof JSONObject) {
			value = getByName.apply((JSONObject) parent, type, lastKey, defaultValue);
		} else {
			value = defaultValue;
		}

		return value;
	}

	private int indexOfLastToken(String pointer) throws MessageBundleException {
		int lastTokenStarts = pointer.lastIndexOf(SLASH) + 1;

		if (lastTokenStarts == pointer.length()) {
			throw new MessageBundleException("Wrong pointer, ending with slash: " + pointer);
		}

		return lastTokenStarts;
	}

	private Object findParent(String pointer, int lastTokenStarts) throws JSONPointerException {
		Object parent = messageBundle;

		if (3 <= lastTokenStarts) {
			parent = messageBundle.query(pointer);
		}

		return parent;
	}

}