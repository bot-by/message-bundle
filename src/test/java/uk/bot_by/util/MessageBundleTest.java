package uk.bot_by.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static uk.bot_by.util.TestColor.Indigo;

@Tag("fast")
public class MessageBundleTest {

	@DisplayName("Bundle names: unordered set of top-level keys")
	@Test
	public void getBundleNames() {
		// when
		Set<String> bundleNames = assertDoesNotThrow(
				() -> MessageBundle.getInstance("uk/bot_by/util/BundleNames.json").getAllBundleNames(),
				"Loaded bundle for BundleNames.json");

		// then
		assertAll("Bundle names",
				() -> assertNotNull(bundleNames, "Set of bundle names exists"),
				() -> assertThat(bundleNames, contains("a", "b", "c", "w", "x", "y", "z")));
	}

	@DisplayName("Could load a message bundle by class")
	@ParameterizedTest
	@ValueSource(classes = {TestClass.class, TestInterface.class})
	public void getInstanceByClass(Class<Object> bundleClass) {
		// when
		assertDoesNotThrow(() -> MessageBundle.getInstance(bundleClass), "Loaded bundle for " + bundleClass.getSimpleName());
	}

	@DisplayName("Could load a message bundle by filename")
	@ParameterizedTest
	@ValueSource(strings = {"a.txt", "uk/bot_by/z.txt"})
	public void getInstanceByString(String baseName) {
		// when
		assertDoesNotThrow(() -> MessageBundle.getInstance(baseName), "Loaded bundle for " + baseName);
	}

	@DisplayName("Throws an exception if JSON has no any bundles")
	@Test
	public void emptyJSON() {
		// when
		MessageBundleException exception = assertThrows(MessageBundleException.class,
				() -> MessageBundle.getInstance("empty.txt"), "Throws an exception for an empty file");

		// then
		assertEquals("Bundles are not found", exception.getMessage(), "Exception message");
	}

	@DisplayName("Throws an exception if a bundle file is not found")
	@Test
	public void fileIsNotFound() {
		// when
		MessageBundleException exception = assertThrows(MessageBundleException.class,
				() -> MessageBundle.getInstance("ItDoesNotExist.txt"), "Throws exception if a file does not exist");

		// then
		assertEquals("ItDoesNotExist.txt is missed", exception.getMessage(), "Exception message");
	}

	@DisplayName("Get another bundle")
	@ParameterizedTest
	@ValueSource(strings = {"x", "y", "z"})
	public void getBundle(String bundleName) {
		// given
		MessageBundle sourceBundle = MessageBundle.getInstance(TestClass.class);

		assumeTrue("x".equals(sourceBundle.getBundleName()));

		// when
		MessageBundle anotherBundle = assertDoesNotThrow(() -> sourceBundle.getBundle(bundleName),
				"Could copy with another bundleName");

		// then
		assertAll("Get copy with another bundleName",
				() -> assertNotNull(anotherBundle, "Bundle is not null"),
				() -> assertEquals(bundleName, anotherBundle.getBundleName(), "Bundle name is equal"));
	}

	@DisplayName("Throws an exception if the source has no that bundle name")
	@ParameterizedTest
	@ValueSource(strings = "w")
	@EmptySource
	@NullSource
	public void bundleNameIsNotFoundInTheSource(String bundleName) {
		// given
		MessageBundle sourceBundle = MessageBundle.getInstance(TestClass.class);

		assumeTrue("x".equals(sourceBundle.getBundleName()));

		// when
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> sourceBundle.getBundle(bundleName), "Could copy with another bundleName");

		// then
		assertThat("Exception message", exception.getMessage(), startsWith("Bundle is not found: "));
	}

	@DisplayName("Get another optional bundle")
	@ParameterizedTest
	@ValueSource(strings = {"x", "y", "z"})
	public void optBundle(String bundleName) {
		// given
		MessageBundle sourceBundle = MessageBundle.getInstance(TestClass.class);

		assumeTrue("x".equals(sourceBundle.getBundleName()));

		// when
		MessageBundle anotherBundle = assertDoesNotThrow(() -> sourceBundle.optBundle(bundleName),
				"Could copy with another bundleName");

		// then
		assertAll("Get copy with another bundleName",
				() -> assertNotNull(anotherBundle, "Bundle is not null"),
				() -> assertEquals(bundleName, anotherBundle.getBundleName(), "Bundle name is equal"));
	}

	@DisplayName("Return the default bundle if an optional bundle is not found")
	@ParameterizedTest
	@ValueSource(strings = "w")
	@EmptySource
	@NullSource
	public void bundleNameIsNotFoundUseDefaultOne(String bundleName) {
		// given
		MessageBundle sourceBundle = MessageBundle.getInstance(TestClass.class);

		assumeTrue("x".equals(sourceBundle.getBundleName()));

		// when
		MessageBundle anotherBundle = assertDoesNotThrow(() -> sourceBundle.optBundle(bundleName),
				"Return default bundle");

		// then
		assertAll("It is a default bundle",
				() -> assertNotNull(anotherBundle, "Bundle is not null"),
				() -> assertEquals("x", anotherBundle.getBundleName(), "Bundle name is equal"));
	}

	@DisplayName("Check type methods")
	@Test
	public void checkTypeMethods() {
		// given
		MessageBundle messageBundle = MessageBundle.getInstance(TestInterface.class);

		assumeTrue("a".equals(messageBundle.getBundleName()));

		// when and then
		assertAll("Check that type methods return correct values",
				() -> assertEquals(TestColor.Green, messageBundle.getEnum(TestColor.class, "enum"), "Enum"),
				() -> assertNull(messageBundle.optEnum(TestColor.class, "optenum"), "Enum, optional value is null"),
				() -> assertEquals(Indigo, messageBundle.optEnum(TestColor.class, "optenum", Indigo),
						"Enum, optional value"),
				() -> assertEquals("a", messageBundle.getString("string"), "String"),
				() -> assertEquals("", messageBundle.optString("optstring"), "String, optional value is am empty string"),
				() -> assertEquals("z", messageBundle.optString("optstring", "z"), "String, optional value is am empty string"),
				() -> assertEquals(1, messageBundle.getInt("int"), "Integer"),
				() -> assertEquals(0, messageBundle.optInt("optint"), "Integer, optional value is zero"),
				() -> assertEquals(123, messageBundle.optInt("optint", 123), "Integer, optional value"),
				() -> assertEquals(12345678900L, messageBundle.getLong("long"), "Long"),
				() -> assertEquals(0L, messageBundle.optLong("optlong"), "Long, optional value is zero"),
				() -> assertEquals(321L, messageBundle.optLong("optlong", 321), "Long, optional value"),
				() -> assertEquals(33.22f, messageBundle.getFloat("float"), "Float"),
				() -> assertEquals(Float.NaN, messageBundle.optFloat("optfloat"), "Float, optional value is NaN"),
				() -> assertEquals(53.98f, messageBundle.optFloat("optfloat", 53.98f), "Float, optional value"),
				() -> assertEquals(123.4567, messageBundle.getDouble("double"), "Double"),
				() -> assertEquals(Double.NaN, messageBundle.optDouble("optdouble"), "Double, optional value is NaN"),
				() -> assertEquals(56.78, messageBundle.optDouble("optdouble", 56.78), "Double, optional value"),
				() -> assertEquals(BigInteger.valueOf(1234567890L), messageBundle.getBigInteger("big integer"),
						"BigInteger"),
				() -> assertEquals(BigInteger.TWO, messageBundle.optBigInteger("optional big integer", BigInteger.TWO),
						"BigInteger, optional value"),
				() -> assertEquals(BigDecimal.TEN, messageBundle.getBigDecimal("big decimal"), "BigDecimal"),
				() -> assertEquals(BigDecimal.ONE, messageBundle.optBigDecimal("optional big decimal", BigDecimal.ONE),
						"BigDecimal, optional value"),
				() -> assertTrue(messageBundle.getBoolean("boolean"), "Boolean"),
				() -> assertFalse(messageBundle.optBoolean("optboolean"), "Boolean, optional value is false"),
				() -> assertTrue(messageBundle.optBoolean("optboolean", true), "Boolean, optional value"),
				() -> assertEquals(Double.valueOf("42.42"), messageBundle.getNumber("number"), "Number"),
				() -> assertNull(messageBundle.optNumber("optnumber"), "Number, optional value is null"),
				() -> assertEquals(123L, messageBundle.optNumber("optnumber", 123L), "Number, optional value is null"));
	}

	@DisplayName("Check query methods")
	@Test
	public void checkQueryMethods() {
		// given
		MessageBundle messageBundle = MessageBundle.getInstance(TestInterface.class);

		assumeTrue("a".equals(messageBundle.getBundleName()));

		// when and then
		assertAll("Check that query methods return correct values",
				() -> assertEquals(TestColor.Indigo, messageBundle.queryEnum(TestColor.class, "/first/enum"),
						"Enum, query value"),
				() -> assertEquals(TestColor.Green, messageBundle.queryEnum(TestColor.class, "/enum"),
						"Enum, query value, top element"),
				() -> assertEquals(TestColor.Yellow, messageBundle.queryEnum(TestColor.class, "/first/second/3"),
						"Enum, query value by index"),
				() -> assertThrows(MessageBundleException.class,
						() -> messageBundle.queryEnum(TestColor.class, "/first2/enum"), "Enum, query non existent pointer"),
				() -> assertEquals(TestColor.Indigo, messageBundle.optQueryEnum(TestColor.class, "/first/enum"),
						"Enum, optional query value, implicit"),
				() -> assertEquals(TestColor.Yellow, messageBundle.optQueryEnum(TestColor.class, "/first/second/3"),
						"Enum, optional query value by index, implicit"),
				() -> assertNull(messageBundle.optQueryEnum(TestColor.class, "/first/enum2"),
						"Enum, optional query value, implicit, default value"),
				() -> assertEquals(TestColor.Indigo, messageBundle.optQueryEnum(TestColor.class, "/first/enum", TestColor.Orange),
						"Enum, optional query value, explicit"),
				() -> assertEquals(TestColor.Yellow, messageBundle.optQueryEnum(TestColor.class, "/first/second/3", TestColor.Orange),
						"Enum, optional query value by index, explicit"),
				() -> assertEquals(TestColor.Orange,
						messageBundle.optQueryEnum(TestColor.class, "/first/enum2", TestColor.Orange),
						"Enum, optional query value, explicit, default value"),
				() -> assertEquals(TestColor.Orange,
						messageBundle.optQueryEnum(TestColor.class, "/first2/enum", TestColor.Orange),
						"Enum, optional query non existent pointer, explicit, default value"),
				() -> assertEquals("z", messageBundle.queryString("/first/string"),
						"String, query value"),
				() -> assertEquals("a", messageBundle.queryString("/string"),
						"String, query value, top element"),
				() -> assertEquals("delta", messageBundle.queryString("/first/second/2"),
						"String, query value by index"),
				() -> assertThrows(MessageBundleException.class, () -> messageBundle.queryString("/first2/string"),
						"String, query non existent pointer"),
				() -> assertEquals("z", messageBundle.optQueryString("/first/string"),
						"String, optional query value, implicit"),
				() -> assertEquals("delta", messageBundle.optQueryString("/first/second/2"),
						"String, optional query value by index, implicit"),
				() -> assertEquals("", messageBundle.optQueryString("/first/string2"),
						"String, optional query value, implicit, default value"),
				() -> assertEquals("z", messageBundle.optQueryString("/first/string", "qwerty"),
						"String, optional query value, explicit"),
				() -> assertEquals("delta", messageBundle.optQueryString("/first/second/2", "qwerty"),
						"String, optional query value by index, explicit"),
				() -> assertEquals("qwerty", messageBundle.optQueryString("/first/string2", "qwerty"),
						"String, optional query value, explicit, default value"),
				() -> assertEquals("qwerty", messageBundle.optQueryString("/first2/string2", "qwerty"),
						"String, optional query non existent pointer, explicit, default value"),
				() -> assertEquals(3, messageBundle.queryInt("/first/int"),
						"Integer, query value"),
				() -> assertEquals(1, messageBundle.queryInt("/int"),
						"Integer, query value, top element"),
				() -> assertEquals(3, messageBundle.optQueryInt("/first/int"),
						"Integer, optional query value, implicit"),
				() -> assertEquals(0, messageBundle.optQueryInt("/first/int2"),
						"Integer, optional query value, implicit, default value"),
				() -> assertEquals(3, messageBundle.optQueryInt("/first/int", -10),
						"Integer, optional query value, explicit"),
				() -> assertEquals(-10, messageBundle.optQueryInt("/first/int2", -10),
						"Integer, optional query value, explicit, default value"),
				() -> assertEquals(-9999999999L, messageBundle.queryLong("/first/long"),
						"Long, query value"),
				() -> assertEquals(12345678900L, messageBundle.queryLong("/long"),
						"Long, query value, top element"),
				() -> assertEquals(-9999999999L, messageBundle.optQueryLong("/first/long"),
						"Long, optional query value, implicit"),
				() -> assertEquals(0L, messageBundle.optQueryLong("/first/long2"),
						"Long, optional query value, implicit, default value"),
				() -> assertEquals(-9999999999L, messageBundle.optQueryLong("/first/long", 123L),
						"Long, optional query value, explicit"),
				() -> assertEquals(123L, messageBundle.optQueryLong("/first/long2", 123L),
						"Long, optional query value, explicit, default value"),
				() -> assertEquals(33.22f, messageBundle.queryFloat("/first/float"),
						"Float, query value"),
				() -> assertEquals(33.22f, messageBundle.queryFloat("/float"),
						"Float, query value, top element"),
				() -> assertEquals(33.22f, messageBundle.optQueryFloat("/first/float"),
						"Float, optional query value, implicit"),
				() -> assertEquals(Float.NaN, messageBundle.optQueryFloat("/first/float2"),
						"Float, optional query value, implicit, default value"),
				() -> assertEquals(33.22f, messageBundle.optQueryFloat("/first/float", 12.34f),
						"Float, optional query value, explicit"),
				() -> assertEquals(12.34f, messageBundle.optQueryFloat("/first/float2", 12.34f),
						"Float, optional query value, explicit, default value"),
				() -> assertEquals(123.4567, messageBundle.queryDouble("/first/double"),
						"Double, query value"),
				() -> assertEquals(123.4567, messageBundle.queryDouble("/double"),
						"Double, query value, top element"),
				() -> assertEquals(123.4567, messageBundle.optQueryDouble("/first/double"),
						"Double, optional query value, implicit"),
				() -> assertEquals(Double.NaN, messageBundle.optQueryDouble("/first/double2"),
						"Double, optional query value, implicit, default value"),
				() -> assertEquals(123.4567, messageBundle.optQueryDouble("/first/double", 33.22),
						"Double, optional query value, explicit"),
				() -> assertEquals(33.22, messageBundle.optQueryDouble("/first/double2", 33.22),
						"Double, optional query value, explicit, default value"),
				() -> assertEquals(BigInteger.TWO, messageBundle.queryBigInteger("/first/big integer"),
						"BigInteger, query value"),
				() -> assertEquals(BigInteger.valueOf(1234567890L), messageBundle.queryBigInteger("/big integer"),
						"BigInteger, query value, top element"),
				() -> assertEquals(BigInteger.TWO, messageBundle.optQueryBigInteger("/first/big integer", BigInteger.ZERO),
						"BigInteger, optional query value"),
				() -> assertEquals(BigInteger.ZERO, messageBundle.optQueryBigInteger("/first/big integer2", BigInteger.ZERO),
						"BigInteger, optional query value, default value"),
				() -> assertEquals(BigDecimal.ONE, messageBundle.queryBigDecimal("/first/big decimal"),
						"BigDecimal, query value"),
				() -> assertEquals(BigDecimal.TEN, messageBundle.queryBigDecimal("/big decimal"),
						"BigDecimal, query value, top element"),
				() -> assertEquals(BigDecimal.ONE, messageBundle.optQueryBigDecimal("/first/big decimal", BigDecimal.TEN),
						"BigDecimal, optional query value"),
				() -> assertEquals(BigDecimal.TEN, messageBundle.optQueryBigDecimal("/first/big decimal2", BigDecimal.TEN),
						"BigDecimal, optional query value, default value"),
				() -> assertTrue(messageBundle.queryBoolean("/first/boolean"), "Boolean, query value"),
				() -> assertTrue(messageBundle.queryBoolean("/boolean"), "Boolean, query value, top element"),
				() -> assertTrue(messageBundle.optQueryBoolean("/first/boolean"),
						"Boolean, optional query value, implicit"),
				() -> assertFalse(messageBundle.optQueryBoolean("/first/boolean2"),
						"Boolean, optional query value, implicit, default value"),
				() -> assertFalse(messageBundle.optQueryBoolean("/first/boolean3", true),
						"Boolean, optional query value, explicit"),
				() -> assertTrue(messageBundle.optQueryBoolean("/first/boolean2", true),
						"Boolean, optional query value, explicit, default value"),
				() -> assertEquals(Double.valueOf("13.13"), messageBundle.queryNumber("/first/number"),
						"Number, query value"),
				() -> assertEquals(Double.valueOf("42.42"), messageBundle.queryNumber("/number"),
						"Number, query value, top element"),
				() -> assertEquals(Double.valueOf("13.13"), messageBundle.optQueryNumber("/first/number"),
						"Number, optional query value"),
				() -> assertEquals(Double.valueOf("12.12"), messageBundle.optQueryNumber("/first/number2", 12.12),
						"Number, optional query value"));
	}

	@DisplayName("Pointer ends with slash")
	@Test
	public void pointerEndsWithSlash() {
		// given
		MessageBundle messageBundle = MessageBundle.getInstance(TestInterface.class);

		assumeTrue("a".equals(messageBundle.getBundleName()));

		// when
		MessageBundleException exceptionA = assertThrows(MessageBundleException.class,
				() -> messageBundle.queryBigInteger("/first/big integer/"), "BigInteger, query value");
		MessageBundleException exceptionB = assertThrows(MessageBundleException.class,
				() -> messageBundle.queryEnum(TestColor.class, "/first/second/3/"), "Enum, query value");

		// then
		assertEquals("Wrong pointer, ending with slash: /first/big integer/", exceptionA.getMessage(), "Exception message");
		assertEquals("Wrong pointer, ending with slash: /first/second/3/", exceptionB.getMessage(), "Exception message");
	}

}