# MessageBundle

[![coverage report](https://gitlab.com/bot-by/message-bundle/badges/master/coverage.svg)](https://gitlab.com/bot-by/message-bundle/-/commits/master)
[![java version](https://img.shields.io/static/v1?label=java&message=11&color=blue&logo=java)](https://www.java.com/download/)

MessageBundle is based on JSON. The JSON file contains top elements. Each elements has name (its key), e.g.
for localisation the name could be equals ISO language code.

Random elements is default but you could change it by `getBundle(String name)`: it creates a new object with references
to all elements and new default bundle. The bundle object is tree of messages (children elements).

## Installation

Add to your project

### Maven

```xml
<dependency>
    <groupId>uk.bot-by.util</groupId>
    <artifactId>message-bundle</artifactId>
    <version>1.2.1</version>
</dependency>
```
### Gradle

```groovy
implementation 'uk.bot-by.util:message-bundle:1.2.1'
```

## Usage

### Get message bundles

```java
// Look for MyClass.json
MessageBundle messageBundle = MessageBundle.getInstance(MyClass.class)
// Look for JSON file by filename
MessageBundle messageBundle = MessageBundle.getInstance("com/example/MyMessages.json")
```

### Get a message

```java
// the simple key:value case
messageBundle.getString("some string");
messageBundle.getInt("some int");
messageBundle.getBigInteger("some big integer");
messageBundle.getEnum(MyEnum.class, "some enum");
messageBundle.optString("some string", "default value");
// using of JSON pointer
messageBundle.queryString("/first/second/0/alpha");
messageBundle.optQueryString("/first/second/0/alpha", "default value");
messageBundle.queryEnum(MyEnum.class, "/first/second/1/beta");
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## History

See [CHANGELOG](CHANGELOG.md)

## License

[Apache License v2.0](LICENSE)  
[![License](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat)](http://www.apache.org/licenses/LICENSE-2.0.html)
